import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import About from "./pages/About/About";
import Invoicing from "./pages/Invoicing/Invoicing";
import ResponsiveDrawer from "./components/ResponsiveDrawer";
import { makeStyles } from "@material-ui/core/styles";
import { PATH_HOME, PATH_VIEW_INVOICE, PATH_ABOUT } from "./global/endpoints";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}));

export default function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Router>
      <ResponsiveDrawer />

      <main className={classes.content}>
        <div className={classes.toolbar} />
        
          <Switch>
            <Route path={PATH_HOME}>
              <Invoicing />
            </Route>

            <Route path={PATH_ABOUT}>
              <About />
            </Route>

            <Route path={PATH_VIEW_INVOICE}>
              <Invoicing />
            </Route>
          </Switch>
      </main>
      </Router>
    </div>
  );
}
