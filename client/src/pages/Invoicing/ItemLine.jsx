import React from "react";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

export default function ItemLine() {
  return (
    <div>
      <Grid item xs={3}>
        <TextField
          multiline
          id="outlined-basic"
          label="Quantity"
          variant="outlined"
          placeholder="1"
        />
      </Grid>
      <Grid item xs={6}>
        <TextField id="outlined-basic" label="Description" variant="outlined" />
      </Grid>
      <Grid item xs={3}>
        <TextField
          id="outlined-basic"
          label="Price"
          variant="outlined"
          placeholder="0.0"
        />
      </Grid>
      <Grid item xs={10}>
        <Button variant="contained" component="label">
          Delete
        </Button>
      </Grid>
    </div>
  );
}
