import React from "react";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import DatePicker from "react-datepicker";
import ItemLine from './ItemLine';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { makeStyles } from '@material-ui/core/styles';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';


const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
  },
}));

export default function Invoicing() {
  const classes = useStyles();
  
  return (
    <Paper>
    <form noValidate autoComplete="off">
        
      <Button
        variant="contained"
        color="default"
        className={classes.button}
        startIcon={<CloudUploadIcon />}
      >
        Upload your Logo
        <input type="file" style={{ display: "none" }} />
      </Button>
      <InputBase
        className={classes.margin}
        defaultValue="INVOICE"
        inputProps={{ 'aria-label': 'naked' }}
      />
        
        <Grid item xs={6}>
          <TextareaAutosize
            multiline
            id="outlined-basic"
            label="From"
            variant="outlined"
            rows="5"
            placeholder="Company Address"
          />
        </Grid>
        <Grid item xs={3}>
          <Button variant="contained" component="label">
            Your Logo
            <input type="file" style={{ display: "none" }} />
          </Button>
        </Grid>
        <Grid item xs={6}>
          <TextField
            multiline
            id="outlined-basic"
            label="Bill To"
            variant="outlined"
            rows="5"
            placeholder="Your customer's billing address"
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            id="outlined-basic"
            label="Invoice #"
            variant="outlined"
            placeholder="Invoice Number"
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            id="outlined-basic"
            label="Invoice Date"
            variant="outlined"
            placeholder="dd/mm/yy"
          />
        </Grid>
        <h4>Description</h4>


        <Grid item xs={10}>
          <Button variant="contained" component="label">
            Add New Item
          </Button>
        </Grid>

        <h4>Subtotal</h4>
        <h4>Total</h4>

        <Grid item xs={10}>
          <TextField
            multiline
            id="outlined-basic"
            label="Terms & Conditions"
            variant="outlined"
            rows="5"
            placeholder="Payment is due within 15 days"
          />
        </Grid>
    </form>
    </Paper>
  );
}
